=pod
=head1 Quadratic Equation
 Ax2 + Bx + C = 0
=cut
use strict;
use warnings;
use 5.12.0;
use bignum;


my $A = shift || 0;
my $B = shift || 0;
my $C = shift || 0;

my $tA=$A;
$tA =~ s/[.]//;

say "tA $tA";

my $tB=$B;
$tB =~ s/[.]//;
say "tB $tB";

my $tC=$C;
$tC =~ s/[.]//;
say "tC $tC";
 
my $count =0;
if ($tA =~ /\D/) { say "$A no es un numero";
	$count += 1; } 
else {say "$A  es numero";	}

if ($tB =~ /\D/) { say "$B no es un numero";
	$count += 1; } 
else {say "$B  es numero";}
if ($tC =~ /\D/) { say "$C no es un numero";
	$count += 1; } 
else {say "$C  es numero"; }
# say "count $count";
if ($count > 0)
{say (STDERR "Input no numerico detectado ");
	exit; }
	
# say $A;
# say $B;
# say $C;
# solucion x = (-b + sqrt(b2 -4 ac))/ 2a
# solucion x = (-b - sqrt(b2 -4 ac))/ 2a

my $fac = sqrt ($B ** $B - 4 * $A * $C) ;
# say "fac $fac";
my $x1 = (- $B + $fac) / (2 * $A) ;
my $x2 = (- $B - $fac) / (2 * $A) ;
 
my $salida = ""; 
if ($x1 eq NaN )   
  {say (STDERR "Solucion invalida x1  ");}
else {$salida = $x1};

if ($x2 eq NaN) 
  {say (STDERR "Solucion invalida x2  ");}
else {$salida = "$salida $x2";}

if($salida ne "") 
 {say "Soluciones validas $salida ";}


