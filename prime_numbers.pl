=pod
=head1 Prime Numbers
=cut
use strict;
use warnings;
use 5.12.0;
use bignum;

say   " Listar los numeros primos menores a uno dado.";
print " Entre un numero entero de 2 a 1000000. ";
my $numero =<STDIN>;
# los numero primos despues del 5 solo acaban en 
#  1, 3, 7 y 9 , no terminan en 5...aceleracion
#  2, 3, 5 y 7 son primos.
# => todo numero no primo es = a la multiplicacion de primos.
# => si es par no es primo, aceleracion.
# comprobar si es divisible por 2 3 5 7 ... si lo es no es primo

my @vecPrimes = (2);
 LOOP:
 for my $num (3 .. $numero) { 
          next LOOP if $num =~ /5$/;
	      next LOOP if $num % 2 == 0;
	#      say "1- $num";
          foreach (@vecPrimes) { 
			  next LOOP if $num % $_ == 0 }
           print "$num ";
          push @vecPrimes, $num;
 }

 
